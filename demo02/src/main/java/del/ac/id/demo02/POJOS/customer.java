package del.ac.id.demo02.POJOS;

public class customer {
	private int id_customer;
	private String nama_customer, alamat;
	
	public customer() {}

	public int getId_customer() {
		return id_customer;
	}

	public void setId_customer(int id_customer) {
		this.id_customer = id_customer;
	}

	public String getNama_customer() {
		return nama_customer;
	}

	public void setNama_customer(String nama_customer) {
		this.nama_customer = nama_customer;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
}
